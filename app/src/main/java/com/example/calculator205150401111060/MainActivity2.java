package com.example.calculator205150401111060;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    TextView bilangan1, bilangan2, Operasi, Hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        bilangan1 = (TextView) findViewById(R.id.tvBilangan1);
        bilangan2 = (TextView) findViewById(R.id.tvBilangan2);
        Operasi = (TextView) findViewById(R.id.tvOperasi);
        Hasil = (TextView) findViewById(R.id.tvHasil);

        String bil1 = getIntent().getStringExtra("bil1");
        String bil2 = getIntent().getStringExtra("bil2");
        String operasi = getIntent().getStringExtra("operasi");
        String hasil = getIntent().getStringExtra("hasil");

        bilangan1.setText(bil1);
        bilangan2.setText(bil2);
        Operasi.setText(operasi);
        Hasil.setText(hasil);
    }
}